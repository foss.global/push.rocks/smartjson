import { tap, expect } from '@pushrocks/tapbundle';

import * as smartjson from '../ts/index.js';

class SomeClass extends smartjson.Smartjson {
  @smartjson.foldDec() thisis: string = 'test';
  constructor() {
    super();
    console.log(this.saveableProperties);
  }
}

let mySomeClass: SomeClass;

tap.test('should create a Foldable extended instance', async () => {
  mySomeClass = new SomeClass();
  expect(mySomeClass).toBeInstanceOf(SomeClass);
  expect(mySomeClass).toBeInstanceOf(smartjson.Smartjson);
});

tap.test('should create a folded object', async () => {
  let foldedObject = mySomeClass.foldToObject();
  expect(foldedObject).property('thisis').toEqual('test');
});

tap.test('should enfold from object', async () => {
  const mySomeClass2 = SomeClass.enfoldFromObject({ thisis: 'test2' });
  expect(mySomeClass2).property('thisis').toEqual('test2');
});

tap.test('should products stable jsons', async () => {
  const jsonString = smartjson.stringify({
    a: 1,
    f: 6,
    b: 3,
    c: 3,
    e: 5,
    d: 4,
  });
  console.log(jsonString);
});

tap.test('should work with base64', async () => {
  const someObject = {
    hi: 'there',
    thisIs: 'awesome',
  };

  const base64Json = smartjson.stringifyBase64(someObject);
  console.log(base64Json);
  const decodedObject = smartjson.parseBase64(base64Json);
  expect(decodedObject).toEqual(someObject);
});

tap.test('stringify should handle plain string', async () => {
  const stringifiedString = smartjson.stringify('hello');
  console.log(stringifiedString);
  expect(stringifiedString).toEqual('"hello"');
  expect(smartjson.parse(stringifiedString)).toEqual('hello');
});

tap.start();
